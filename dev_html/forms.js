// forms.js

const init = function(){
	document.getElementById('button-avbryt').addEventListener('click', reset);
	document.getElementById('button-generer').addEventListener('click', generer);
}

const reset = function(ev){
	ev.preventDefault();
	
	document.getElementById('form-user').reset();
}