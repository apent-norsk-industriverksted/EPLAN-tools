from dev_python.variables import EPLANconf, ELEMENTS
from os.path import isdir

conf = EPLANconf()

def test_eplan_read_conf():
    # Verify that all elements are in the dictionary
    for key in ELEMENTS:
        assert conf[key]

def test_eplan_read_full_sysconf():
    # Verify that all elements has an actual directory in the system
    for key in ELEMENTS:
        assert conf.path_raw(key)

def test_eplan_sysconf_directoies_exsist():
    # Verify that all elements has an actual directory in the system
    for key in ELEMENTS:
        assert isdir(conf[key])
