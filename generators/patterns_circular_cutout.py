"""
# Usage:
- Run this script from the "EPLAN-tools" project root folder.
- Cutouts are now generated at "Add-ons\\Schemes\\cutouts\\"
- Changes can now be evaluated, tested and published

# Description:
Reads cutout templates and generates cutout files.
This approach reduces errors compared to creating cutouts manually in the EPLAN user interface.

Cutout files can be imported to EPLAN. (Parts management, Extras, Import...)

"""

from jinja2 import Environment, Template

cutout_diameters= [4.5,12,16,20,25,32,40,50,63]
env = Environment(keep_trailing_newline=True)

with open("templates\\pattern_circular_cutout.xml", mode="r") as file_:
        template = env.from_string(file_.read())
file_.close()

for diameter in cutout_diameters:
    text = template.render(cutout_diameter=diameter)
    with open('Add-ons\\Schemes\\cutouts\\D'+str(diameter)+'.xml', mode='w') as outfile:
        outfile.write(text)
    outfile.close()

thread_diameters= [4,5,6,8,32,40,50,63]

with open("templates\\pattern_threaded_circular_cutout.xml", mode="r") as file_:
        template = env.from_string(file_.read())
file_.close()
for diameter in thread_diameters:
    text = template.render(cutout_diameter=diameter)
    with open('Add-ons\\Schemes\\cutouts\\M'+str(diameter)+'.xml', mode='w') as outfile:
        outfile.write(text)
    outfile.close()
