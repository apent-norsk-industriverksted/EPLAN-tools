using System.Diagnostics;

public class UtilitiesToolbar{

	[DeclareAction("checkInProject")]
	public void checkInProject(){
    bool bResult;
    ActionCallingContext context;
    // TODO: Find a better path or remove variable
    string localFileArea = PathMap.SubstitutePath(@"$(ENVVAR_LOCALAPPDATA)\EPLAN-grunndata");
    string projectCacheFolder = localFileArea+@"\projectCache";
    string projectHistoryFolder = PathMap.SubstitutePath(localFileArea+@"\history\$(LOCALDATE)-$(LOCALTIME)-check-in");
    
    //
    // Copy to log and delete cache
    //
    // Create a folders if it is not there already
    System.IO.Directory.CreateDirectory(projectCacheFolder);
    System.IO.Directory.CreateDirectory(projectHistoryFolder);

    // Copy excisting projectCache-folder into history
    if (System.IO.Directory.Exists(projectCacheFolder)){
        string[] files = System.IO.Directory.GetFiles(projectCacheFolder);
        string fileName, destFile;
        // Copy files. Owerwrite if they exist.
        foreach (string file in files)
          {
            fileName = System.IO.Path.GetFileName(file);
            destFile = System.IO.Path.Combine(projectHistoryFolder, fileName);
            System.IO.File.Copy(file, destFile, true);
          }
        // Delete after copy
        System.IO.Directory.Delete(projectCacheFolder, true);
    }else {
        MessageBox.Show("Source path does not exist\n\n"+projectCacheFolder, "Check in");
    }

    //
    // Back up project to the projectCache-folder
    //
    bResult = false;
		context = new ActionCallingContext ();
		context.AddParameter("type","PROJECT");
		context.AddParameter("destinationpath",projectCacheFolder);
		context.AddParameter("backupamount","BACKUPAMOUNT_ALL");
		context.AddParameter("inclextdocs","0");
		context.AddParameter("inclimages","1");
		context.AddParameter("compressprj","0");
		context.AddParameter("backupmedia","DISK");
		context.AddParameter("backupmethod","BACKUP");
		bResult = new CommandLineInterpreter().Execute("backup",context);
		if(!bResult){
      MessageBox.Show("Project backup failed\n\n"+localFileArea, "Check in");
      }

  //
  // Export the bill of materials (BOM)
  //
  bResult = false;
	context = new ActionCallingContext ();
	context.AddParameter("TYPE","EXPORT");
  context.AddParameter("EXPORTFILE",projectCacheFolder+@"\_BOM.xml");
  context.AddParameter("FORMAT","XPamExportXml");
	bResult = new CommandLineInterpreter().Execute("partslist",context);
	if(!bResult){
     MessageBox.Show("BOM export failed\n\n"+projectCacheFolder+@"\_BOM.xml", "Check in");
    }
  }

  [DeclareMenu]
	public void checkInProjectMenu(){
    Eplan.EplApi.Gui.Menu oMenu = new Eplan.EplApi.Gui.Menu();
		oMenu.AddMenuItem("Check in project","checkInProject");
  }
}
