# Innføring

Målet med et makroprosjekt er å kunne lage produksjonsunderlag raskt og med god
kvalitet. Makroprosjektet genererer makroer som EPLAN kan bruke.

Her kommer noen spesifikke forslag til hvordan et makroprosjekt skal utvikles.

Filstier i dette dokumentet er relative til **C:/Users/Public/EPLAN/{{company-code.lowercase}}-data-eplan**

# Forslag til strukturering

Vi tar nå eierskap til to mapper. Mappene **Macros/{{company-code}}/WINDOW** og 
**Macros/{{company-code}}/SYMBOL**. Bedrifter av små og mellomstor størrelse
trenger ikke undermapper. Vi tillater at makroprosjektet gjør endringer 
i disse mappene. Vi tillater ikke at makroprosjektet gjør endringer i andre mapper.
Slik unngår vi å konflikter med dataportalen og nye utgaver av EPLAN.

Symbolmakroer har etternavn **.ems** og legges i **Macros/{{company-code}}/SYMBOL**
mappen. Vindumakroer har etternavn **.ema** og legges i **Macros/{{company-code}}/WINDOW**
mappen. 

Et godt utgangspunkt er å starte makroens fornavn med EPLAN sitt delenummer.
3D makroer skal ende med **_3D** på slutten
av fornavnet.

Eksempel: 3D modell av Rittall sin RIT.1468500 legge her

```
SYMBOL/RIT.1468500_3D.ema
```

Ja, det er sant at RIT.1468500 finnes i dataportalen allerede, men makroer for
Rittal sin KS serie er problematiske fordi de tar definert **placement area** i
bunn av skapet. Bunn av dette skapet står ikke 90 grader på dør og bakside.
Dette gjør at komponenter som blir satt i skapsiden blir vridd noen grader.
