Prosjektmalen er et viktig verktøy for å fange opp gode standardinstillinger.

# Produksjonsunderlag til de som skal borre i skapet

&EED1

# Produksjonsunderlag til skapbygger

Filnavn: **&EED2.pdf**

## Oversiktsbilde over hele tavlen

Modell av tavlen sett på skrå. Ingen rapporter.

## Oversiktsbilde over bakplate(r)

Model view: Scheme for automatic dimensioning	**ANI All 1st level items**

Enclosure legend form	**F18_ANS-CUT_V01.f18**

Model view: Selection scheme	**ANS Panel mount**

- Function range	Mechanics: System accessories
- Function range	Mechanics: Current distribution

## Detaljert 