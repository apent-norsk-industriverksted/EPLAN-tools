# Eplan deledatabase

Erfaring viser at det er mye jobb å vedlikeholde et stort antall felter i deledatabasen. Denne oversikten fremhever feltene som er viktige for å få utnyttet mulighetene i EPAN.

|UI navn|Beskrivelse|Egenskaper
|---|---|---|
|Part number|Standardfelt. Eplan sin unike referanse. Dataportalen dikterer litt av hva som fylles i disse feltene.|Streng|
|Designation 1|Standardfelt. Delens beskrivelse. Skrives på delelisten.|Streng, flerspråklig|
|ERP number|Valgfritt felt. Konstruktører liker ofte å lage sitt eget delenummer. Dette kan gi flere fordeler. (frihet til å bytte ut produkt, ryddigere deleliste, lekker mindre info om produktet etc.)|Streng, flerspråklig|
|Quantity unit|Standardfelt. Bruk nedtrekksmenyen og slett duplikater hvis rare varianter sniker seg inn. Det er fornuftig å velge stk som basisenhet. Også for kabler fordi de har lengde i et annet felt. |Streng, flerspråklig|
