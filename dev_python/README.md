# Python scripts

EPLAN can output bill of material in a big .xml file. This file is complex. The **bom_generator_merge, bom_generator_json and bom_generator** scripts all convert the EPLAN bill of material to another format.

# Compiling python files:

- Python 3.7.6

```cmd
pyinstaller --onefile bom_generator_merge.py
pyinstaller --onefile bom_generator_json.py
pyinstaller --onefile bom_generator_xlsx.py
```

# Bug i bom_generator_xlsx.py

- Linjer blir ikke med hvis leverandør mangler
- Linjer blir ikke med hvis fabrikant mangler
- Ordrenummer flettes inn fra prosjektet. Dette er en feilkilde.