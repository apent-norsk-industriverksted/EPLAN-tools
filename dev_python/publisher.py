import win32com.client as win32
from jinja2 import Template

def _Emailer(text, subject, recipient, auto=False):
    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = recipient
    mail.Subject = subject
    mail.Body = text
    if auto:
        mail.send
    else:
        mail.Display(True)

def status_report(project_name, user_name, user_email):
    with open("templates\\email_request_inspection.txt", mode="r", encoding="utf-8") as file_:
        template = Template(file_.read())

    text = template.render(user_name=user_name, project_name=project_name)
    subject = project_name + ", klar til inspeksjon"

    _Emailer(text, subject, user_email)

if __name__ == "__main__":
    status_report(
        project_name="Prosjektnavn", 
        user_name="Anton", 
        user_email="anton.posen@mailinator.com")
