"""
# Usage:
bom_generator -i1 <BOM_input.xml> -I2 <pricelist_input.xlsx> -o <BOM_output.xlsx>

# Description:
Reads partlist from EPLAN in .xml format (Bill of materials navigator, høyre museknapp, Export...)

Generates BOM in .xlsx format. First sheet (Total) has all project parts. Then sheets are generated per location.

"""

from voluptuous import All, ExactSequence, Coerce, Error, IsFile, Length, REMOVE_EXTRA, Schema, Optional
import xml.etree.ElementTree as ET
import pandas as pd

def bom_generator(input_path_bom_xml, input_path_pricelist, output_path_bom_xml):
    # Read bill of material in XML format
    xml_tree_of_bom = ET.parse(input_path_bom_xml)
    xml_root_of_input_bom = xml_tree_of_bom.getroot()
    xml_input_bom_parts = xml_root_of_input_bom.findall("./device/part")

    parts_valid = []
    parts_invalid = []
    fullocations_unique = []

    # Define schema for validation of XML input
    input_bom_xml_schema = Schema({
        'P_ARTICLEREF_PARTNO': All(str, Length(min=1)),
        Optional('P_ARTICLE_DESCR1', default=''): Coerce(str),
        Optional('P_ARTICLE_SUPPLIER', default=''): Coerce(str),
        Optional('P_ARTICLE_MANUFACTURER', default=''): Coerce(str),
        Optional('P_ARTICLE_ORDERNR', default=''): Coerce(str),
        Optional('P_ARTICLEREF_COUNT', default=0.0): Coerce(float),
        Optional('P_ARTICLE_PARTIAL_LENGTH_IN_PROJECT_UNIT', default=0.0): Coerce(float),
        Optional('P_DESIGNATION_FULLLOCATION_WITHPREFIX', default='+'): Coerce(str),
        }, required=True, extra=REMOVE_EXTRA)

    column_labels = {
        'P_DESIGNATION_FULLLOCATION_WITHPREFIX': 'Location',
        'P_ARTICLEREF_PARTNO': 'Part number',
        'P_ARTICLE_DESCR1': 'Designation 1',
        'P_ARTICLE_SUPPLIER': 'Supplier',
        'P_ARTICLE_MANUFACTURER': 'Manufacturer',
        'P_ARTICLE_ORDERNR': 'Order number',
        'P_ARTICLEREF_COUNT': 'Quantity',
        'P_ARTICLE_PARTIAL_LENGTH_IN_PROJECT_UNIT': 'Length'}

    # Count accepted lines (i) and rejected lines (j)
    i=0
    j=0
    
    # Validate XML input
    for part_for_test in xml_input_bom_parts:
        try:
            input_bom_xml_schema(part_for_test.attrib)
        except Error as e:
            parts_invalid.append(part_for_test.attrib['P_FUNC_IDENTDEVICETAG']+' -- '+str(e))
            j=j+1 # Count invalid parts
        else:
            parts_valid.append(input_bom_xml_schema(part_for_test.attrib))
            i=i+1 # Count valid parts
    df = pd.DataFrame(parts_valid)
    
    # Change to muman readable labels for XML input
    df.rename(columns=column_labels, inplace=True)

    #Debug loop printing causes for rejected parts:
    #for part_invalid in parts_invalid:
    #    print(part_invalid)

    # Find unique locations
    fullocations_unique = df.Location.sort_values().unique()

    #TODO: READ AND VALIDATE PRICELIST HERE

    # Read XLSX pricelist
    input_pricelist_fields = ['Part number', 'Quantity unit', 'Unit cost', 'Unit cost date']
    df_input_pricelist = pd.read_excel(
        input_path_pricelist,
        header=1,
        usecols=input_pricelist_fields,
        keep_default_na=False,
        )

    # Define schema for validation of XLSX pricelist
    input_pricelist_schema = Schema({
        'Part number': Coerce(str),
        'Quantity unit': Coerce(str),
        'Unit cost': Coerce(float),
        Optional('Unit cost date', default=''): Coerce(str),
    }, required=True, extra=REMOVE_EXTRA)

    for pricelist_element_index, pricelist_element in df_input_pricelist.iterrows():
        # Verify pricelist
        try: 
            df_input_pricelist.loc[pricelist_element_index] = input_pricelist_schema(pricelist_element.to_dict())
        except Error as e:
            print(pricelist_element.to_dict(), e)
            exit(-1)

    # Add new column to pricelist
    df_input_pricelist.insert(4, "Total cost", 0.0, allow_duplicates=True)

    # Merge BOM and pricelist
    df = pd.merge(df, df_input_pricelist, on="Part number", how="left")
    
    # Pricelist may not have all products. Adding default values after merge.
    df['Quantity unit'].fillna('--MISSING--', inplace=True)
    df['Unit cost'].fillna(0.0, inplace=True)
    df['Unit cost date'].fillna('', inplace=True)
    df['Total cost'].fillna(0.0, inplace=True)

    # Group
    df = df.groupby(by=['Location' ,'Part number', 'Designation 1', 'Supplier', 'Manufacturer', 'Order number', 'Quantity unit', 'Unit cost', 'Unit cost date'], as_index=False)['Quantity', 'Length'].sum()

    lines_missing_price = 0
    
    for index, row in df.iterrows():
        # Isolate the English language
        designation_string = row['Designation 1']
        if (designation_string.find('en_US')>-1):
            designation_startpos = designation_string.find('en_US')+6
        elif (designation_string.find('??_??')>-1):
            designation_startpos = designation_string.find('??_??')+6
        else:
            designation_startpos=0
        designation_endpos = designation_string.find(';', designation_startpos)
        df.loc[index,'Designation 1'] = designation_string[designation_startpos:designation_endpos]

        # Calculate total cost
        if (row['Quantity unit'] == 'm'):
            df.loc[index,'Total cost'] = row['Unit cost']*row['Length']
        else:
            df.loc[index,'Total cost'] = row['Unit cost']*row['Quantity']
        
    # Writer
    writer = pd.ExcelWriter(output_path_bom_xml, engine='xlsxwriter')

    # Create sheet for sum of parts
    df_location = df.groupby(by=['Part number', 'Designation 1', 'Supplier', 'Manufacturer', 'Order number', 'Quantity unit', 'Unit cost', 'Unit cost date'], as_index=False)['Quantity', 'Length', 'Total cost'].sum()
    df_location.sort_values(by=['Supplier', 'Manufacturer', 'Order number'], inplace=True)
    df_location.to_excel(writer, sheet_name='Total', index=False)
    writer.sheets['Total'].set_column('A:A', 30)
    writer.sheets['Total'].set_column('B:B', 55)
    writer.sheets['Total'].set_column('C:C', 10)
    writer.sheets['Total'].set_column('D:F', 15)
    writer.sheets['Total'].set_column('G:G', 10)
    writer.sheets['Total'].set_column('H:H', 20)
    writer.sheets['Total'].set_column('I:I', 10)

    # Create and adjust sheet for every location
    for location in fullocations_unique:
        df_location = df[df['Location'] == location]
        df_location = df_location.filter(items=['Part number', 'Designation 1', 'Supplier', 'Manufacturer', 'Order number', 'Quantity unit', 'Unit cost', 'Unit cost date', 'Quantity', 'Length', 'Total cost'])
        df_location.sort_values(by=['Supplier', 'Manufacturer', 'Order number'], inplace=True)

        df_location.to_excel(writer, sheet_name=location, index=False)
        writer.sheets[location].set_column('A:A', 30)
        writer.sheets[location].set_column('B:B', 55)
        writer.sheets[location].set_column('C:C', 10)
        writer.sheets[location].set_column('D:F', 15)
        writer.sheets[location].set_column('G:G', 10)
        writer.sheets[location].set_column('H:H', 20)
        writer.sheets[location].set_column('I:I', 10)

    writer.save()
    print('Export finished, ',i,'elements exported,', j, 'elements ignored, ',lines_missing_price,' elements had invalid price info')

if __name__=='__main__':
    import sys, logging
    # Rules for data input
    schema = Schema(ExactSequence([str, '-i1', IsFile(str), '-i2', IsFile(str), '-o', str]))
    try:
        # Validate input_path_bom_xml
        validator = Schema('-i1')
        last_input = sys.argv[1]
        validator(last_input)

        validator = Schema(IsFile(str))
        last_input = sys.argv[2]
        input_path_bom_xml=validator(last_input)

        # Validate input_path_pricelist
        validator = Schema('-i2')
        last_input = sys.argv[3]
        validator(last_input)

        validator = Schema(IsFile(str))
        last_input = sys.argv[4]
        input_path_pricelist=validator(last_input)

        # Validate output_path_bom_xml
        validator = Schema('-o')
        last_input = sys.argv[5]
        arg3=validator(last_input)

        validator = Schema(IsFile(str))
        last_input = sys.argv[6]
        output_path_bom_xml =   last_input     

    except Error as e:
      print(last_input, e)
      # Print usage tips and exit if input is not valid
      logging.critical('tips for correct usage: bom_generator -i1 <BOM_input.xml> -i2 <pricelist_input.xlsx> -o <BOM_output.xlsx>')
      exit(2)
    else:
        # Input is validated. Calling function.
        bom_generator(input_path_bom_xml, input_path_pricelist, output_path_bom_xml)
