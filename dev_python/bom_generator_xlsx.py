"""
# Usage:
bom_generator_xlsx -i <inputfile.XML> -o <outputfile.XLSX>

# Description:
Reads partlist from EPLAN in .xml format (Bill of materials navigator, høyre museknapp, Export...)

Generates BOM in .xlsx format. First sheet (Total) has all project parts. Then sheets are generated per location.

"""

from voluptuous import All, ExactSequence, Coerce, Error, IsFile, Length, REMOVE_EXTRA, Schema
import xml.etree.ElementTree as ET
import pandas as pd

def bom_generator_xlsx(input_path_bom_xml, output_path_bom_xml):
    # Read XML
    tree = ET.parse(input_path_bom_xml)
    partList = tree.getroot()
    parts_valid = []
    parts_invalid = []
    fullocations_unique = []

    # Define schema for validation
    schema = Schema({
        'P_DESIGNATION_FULLLOCATION_WITHPREFIX': str,
        'P_ARTICLEREF_PARTNO': All(str, Length(min=1)),
        'P_ARTICLE_DESCR1': str,
        'P_ARTICLE_SUPPLIER': str,
        'P_ARTICLE_MANUFACTURER': str,
        'P_ARTICLE_ORDERNR': All(str, Length(min=1)),
        'P_ARTICLE_QUANTITY_IN_PROJECT_UNIT': Coerce(float),
        'P_ARTICLEREF_CABLE_LENGTH_SUM': Coerce(float)
        }, required=True, extra=REMOVE_EXTRA)

    column_labels = {
        'P_DESIGNATION_FULLLOCATION_WITHPREFIX': 'Location',
        'P_ARTICLEREF_PARTNO': 'Part number',
        'P_ARTICLE_DESCR1': 'Designation 1',
        'P_ARTICLE_SUPPLIER': 'Supplier',
        'P_ARTICLE_MANUFACTURER': 'Manufacturer',
        'P_ARTICLE_ORDERNR': 'Order number',
        'P_ARTICLE_QUANTITY_IN_PROJECT_UNIT': 'Quantity',
        'P_ARTICLEREF_CABLE_LENGTH_SUM': 'Length'}

    # Count accepted lines (i) and rejected lines (j)
    i=0
    j=0

    # Loop and validate
    for element in partList:
        if(element.tag == 'device'):
            for part in element:
                try:
                    schema(part.attrib)
                except Error as e:
                    parts_invalid.append(element.attrib['P_ARTICLEREF_IDENTNAME']+': '+str(e))
                    j=j+1 # Count invalid parts
                    print('Bad part:', element.attrib['P_ARTICLEREF_IDENTNAME'])
                else:
                    parts_valid.append(schema(part.attrib))
                    i=i+1 # Count valid parts
    df = pd.DataFrame(parts_valid)

    # Find unique locations
    fullocations_unique = df.P_DESIGNATION_FULLLOCATION_WITHPREFIX.sort_values().unique()

    # Change labels
    df.rename(columns=column_labels, inplace=True)

    # Group
    df = df.groupby(by=['Location' ,'Part number', 'Designation 1', 'Supplier', 'Manufacturer', 'Order number'], as_index=False)['Quantity', 'Length'].sum()

    # Isolate the English language
    for index, row in df.iterrows():
        designation_string = row['Designation 1']#.split(';')
        if (designation_string.find('en_US')>-1):
            designation_startpos = designation_string.find('en_US')+6
        elif (designation_string.find('??_??')>-1):
            designation_startpos = designation_string.find('??_??')+6
        else:
            designation_startpos=0
        designation_endpos = designation_string.find(';', designation_startpos)
        df.loc[index,'Designation 1'] = designation_string[designation_startpos:designation_endpos]

    # Writer
    writer = pd.ExcelWriter(output_path_bom_xml, engine='xlsxwriter')

    # Create sheet for sum of parts
    df_location = df.groupby(by=['Part number', 'Designation 1', 'Supplier', 'Manufacturer', 'Order number'], as_index=False)['Quantity', 'Length'].sum()
    df_location.sort_values(by=['Supplier', 'Manufacturer', 'Order number'], inplace=True)
    df_location.to_excel(writer, sheet_name='Total', index=False)
    writer.sheets['Total'].set_column('A:A', 30)
    writer.sheets['Total'].set_column('B:B', 55)
    writer.sheets['Total'].set_column('C:C', 10)
    writer.sheets['Total'].set_column('F:G', 10)
    writer.sheets['Total'].set_column('D:E', 15)

    # Create and adjust sheet for every location
    for location in fullocations_unique:
        df_location = df[df['Location'] == location]
        df_location = df_location.filter(items=['Part number', 'Designation 1', 'Supplier', 'Manufacturer', 'Order number','Quantity', 'Length'])
        df_location.sort_values(by=['Supplier', 'Manufacturer', 'Order number'], inplace=True)

        df_location.to_excel(writer, sheet_name=location, index=False)
        writer.sheets[location].set_column('A:A', 30)
        writer.sheets[location].set_column('B:B', 55)
        writer.sheets[location].set_column('C:C', 10)
        writer.sheets[location].set_column('D:E', 15)
        writer.sheets[location].set_column('F:G', 10)

    writer.save()
    print('Export finished, ',i,'elements exported,', j, 'elements ignored.')

if __name__=='__main__':
    import sys, logging
    # Rules for data input
    schema = Schema(ExactSequence([str, '-i', IsFile(str), '-o', str]))
    try:
        args=schema(sys.argv)
    except Error as e:
        # Print usage tips and exit if input is not valid
        logging.critical('tips for correct usage: bom_generator_xlsx -i <inputfile.xml> -o <outputfile.xlsx>')
        exit(2)
    else:
        # Input is validated. Calling function.
        arg0, arg1, input_path_bom_xml, arg2, output_path_bom_xml = args
        bom_generator_xlsx(input_path_bom_xml, output_path_bom_xml)
